# CONWAY GAME OF LIFE
This application gives you a set of seeds that you can choose. It will gives you simulation about conway game of life.

## Rules
1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

## To Run App
**With Text File**
```
sh start.sh input.txt
```
**Without Text File**
```
sh start.sh
```

# Getting Started

## Running in Vagrant
Install virtualbox & vagrant

Vagrant plugins:
```
vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-disksize
```

For initial setup:
```
vagrant up --provision
```

Subsequently, use:
```
vagrant up
```

To stop a vagrant box, run:
```
vagrant halt
```

To use app inside vagrant, run:
```
vagrant ssh
```

Navigate to the working directory inside Vagrant:
```
cd /srv/game_of_life
```

### Building the app
To build the app, navigate to the project folder, and build:
```
gradle build
```

### Testing the app
To test the app, navigate to the project folder and run the following:
```
gradle test
```
