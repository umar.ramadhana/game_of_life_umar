import com.bootcamp.conway.ConwayGameApp;
import com.bootcamp.conway.Template;
import com.bootcamp.conway.Universe;

import java.io.IOException;

/*
 * Entry point for user
 */
public class App {
    public static void main(String[] args) throws InterruptedException, IOException {
        ConwayGameApp conwayGameApp = new ConwayGameApp();
        conwayGameApp.start(args);
    }
}
