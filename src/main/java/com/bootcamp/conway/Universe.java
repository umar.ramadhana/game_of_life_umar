package com.bootcamp.conway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// Representate universe where living cell lives
public class Universe {
    private ArrayList<Cell> cellPopulation = new ArrayList<>();
    private HashMap<String, Integer> neighbourCountMap = new HashMap<String, Integer>();

    private int mapMinX = 32768;
    private int mapMinY = 32768;
    private int mapMaxX = -32768;
    private int mapMaxY = -32768;

    public ArrayList<Cell> seedCells(int[][] seeds) {
        cellPopulation.clear();
        for (int[] seed : seeds) {
            Cell cell = new Cell(seed[0], seed[1]);
            cellPopulation.add(cell);
        }
        checkEdgeCellPosition();
        return cellPopulation;
    }

    private void checkEdgeCellPosition() {
        for (Cell cell : cellPopulation) {
            if (cell.position()[0] < mapMinX) mapMinX = cell.position()[0];
            if (cell.position()[0] > mapMaxX) mapMaxX = cell.position()[0];
            if (cell.position()[1] < mapMinY) mapMinY = cell.position()[1];
            if (cell.position()[1] > mapMaxY) mapMaxY = cell.position()[1];
        }
    }

    public String printUniverse() {
        String output = "";
        for (int y = mapMinY; y <= mapMaxY; y++) {
            for (int x = mapMinX; x <= mapMaxX; x++) {
                int cellFound = 0;
                for (Cell cell : cellPopulation) {
                    if (cell.position()[0] == x && cell.position()[1] == y) {
                        cellFound = 1;
                        break;
                    }
                }
                if (cellFound == 1) {
                    output += "#";
                } else {
                    output += "-";
                }
            }
            output += "\n";
        }
        return output;
    }

    public void checkAllNeighbourCount() {
        for (Cell cell : cellPopulation) {
            int cellXCoordinate = cell.position()[0];
            int cellYCoordinate = cell.position()[1];

            for (int y = cellYCoordinate - 1; y <= cellYCoordinate + 1; y++) {
                for (int x = cellXCoordinate - 1; x <= cellXCoordinate + 1; x++) {
                    if (y == cellYCoordinate && x == cellXCoordinate) {
                        String key = x + " " + y + " " + 1;
                        if (neighbourCountMap.get(key) == null) {
                            neighbourCountMap.put(key, 1);
                        } else {
                            neighbourCountMap.put(key, neighbourCountMap.get(key) + 1);
                        }
                    } else {
                        String key = x + " " + y + " " + 0;
                        if (neighbourCountMap.get(key) == null) {
                            neighbourCountMap.put(key, 1);
                        } else {
                            neighbourCountMap.put(key, neighbourCountMap.get(key) + 1);
                        }
                    }
                }
            }
        }
    }

    public String printNeighbouring() {
        String output = "";
        for (Map.Entry<String, Integer> cell : neighbourCountMap.entrySet()) {
            String key = cell.getKey();
            int value = cell.getValue();

            output += key + " = " + value + "\n";
        }
        return output;
    }

    public int[][] translateNeighbouringToNewSeed() {
        checkAllNeighbourCount();

        ArrayList<int[]> newLivingCell = new ArrayList<>();

        for (Map.Entry<String, Integer> cell : neighbourCountMap.entrySet()) {
            String[] key = cell.getKey().split(" ");
            int value = cell.getValue();

            if (key[2].equals("1")) {
                int realValue = value;
                if (neighbourCountMap.get(key[0] + " " + key[1] + " 0") != null) {
                    realValue = neighbourCountMap.get(key[0] + " " + key[1] + " 0");
                }

                if (realValue == 2 || realValue == 3) {
                    int[] livingCell = {Integer.parseInt(key[0]), Integer.parseInt(key[1])};
                    newLivingCell.add(livingCell);
                }
            } else {
                int realValue = value;

                if (realValue == 3 && neighbourCountMap.get(key[0] + " " + key[1] + " 1") == null) {
                    int[] livingCell = {Integer.parseInt(key[0]), Integer.parseInt(key[1])};
                    newLivingCell.add(livingCell);
                }
            }
        }

        neighbourCountMap.clear();
        return newLivingCell.toArray(new int[][]{});
    }
}
