package com.bootcamp.conway;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

// Main Menu for user
public class ConwayGameApp {
    private ArrayList<int[]> seedsFromText = new ArrayList<>();
    private int[][] translatedSeeds;

    public void start(String[] args) throws IOException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        Template template = new Template();
        Universe universe = new Universe();

        if (args.length > 0) {
            initiateFromFile(args[0], universe);
        } else {
            initiateFromUserInput(universe);
        }
        System.out.println(universe.printUniverse());
        System.out.println();

        System.out.print("INPUT CYCLES [0 for infinite] : ");
        int numberCycle = sc.nextInt();
        if (numberCycle == 0) {
            while (true) {
                System.out.println("\u001b[2J");
                int[][] newSeed = universe.translateNeighbouringToNewSeed();
                universe.seedCells(newSeed);
                System.out.println(universe.printUniverse());
                Thread.sleep(100);
            }
        } else {
            for (int i = 1; i <= numberCycle; i++) {
                System.out.println("\u001b[2J");
                int[][] newSeed = universe.translateNeighbouringToNewSeed();
                universe.seedCells(newSeed);
                System.out.println(universe.printUniverse());
                Thread.sleep(100);
            }
        }
    }

    private void initiateFromUserInput(Universe universe) {
        Scanner sc = new Scanner(System.in);
        Template template = new Template();

        System.out.println("1. STILL LIFE");
        System.out.println("2. BLINKER");
        System.out.println("3. GLIDER");
        System.out.println("4. REVERSE GLIDER");
        System.out.println("5. GLIDER GUN");
        System.out.println("6. SPACESHIP");
        System.out.println("7. SPACESHIP UP");
        System.out.println("8. SPACESHIP LEFT");
        System.out.print("CHOOSE TEMPLATE : ");
        int option = sc.nextInt();

        switch (option) {
            case 1:
                universe.seedCells(template.STILL_LIFE());
                break;
            case 2:
                universe.seedCells(template.BLINKER());
                break;
            case 3:
                universe.seedCells(template.GLIDER());
                break;
            case 4:
                universe.seedCells(template.GLIDERREVERSE());
                break;
            case 5:
                universe.seedCells(template.GLIDERGUN());
                break;
            case 6:
                universe.seedCells(template.SPACESHIP());
                break;
            case 7:
                universe.seedCells(template.SPACESHIPUP());
                break;
            case 8:
                universe.seedCells(template.SPACESHIPLEFT());
                break;
        }
    }

    private void initiateFromFile(String arg, Universe universe) throws IOException {
        File file = new File(arg);
        FileInputStream fis = new FileInputStream(file);
        char ch;
        int x = 0;
        int y = 0;
        while (fis.available() > 0) {
            ch = (char) fis.read();
            if (ch == '.') {
                x += 1;
            } else if (ch == 'O') {
                int[] seeds = {x, y};
                seedsFromText.add(seeds);
                x += 1;
            } else if (ch == '\n') {
                y += 1;
                x = 0;
            }
        }
        translatedSeeds = seedsFromText.toArray(new int[][]{});
        universe.seedCells(translatedSeeds);
    }
}
