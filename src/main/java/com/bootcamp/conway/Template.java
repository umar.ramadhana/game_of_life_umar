package com.bootcamp.conway;

// To load seeds template
public class Template {
    public Template() {
    }

    public int[][] SANDBOX() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2},
                {3, 3},
                {2, 3},
                {4, 4},
                {5, 5}
        };
        return seedsMap;
    }

    public int[][] STILL_LIFE() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {1, 3},
                {2, 3}
        };
        return seedsMap;
    }

    public int[][] BLINKER() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {3, 2}
        };
        return seedsMap;
    }

    public int[][] BLINKER2() {
        int[][] seedsMap = {
                {3, 1},
                {3, 2},
                {3, 3}
        };
        return seedsMap;
    }

    public int[][] GLIDER() {
        int[][] seedsMap = {
                {1, 3},
                {2, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        return seedsMap;
    }

    public int[][] GLIDERREVERSE() {
        int[][] seedsMap = {
                {1, 3},
                {0, 4},
                {0, 5},
                {1, 5},
                {2, 5}
        };
        return seedsMap;
    }

    public int[][] SPACESHIP() {
        int[][] seedsMap = {
                {0, 4},
                {3, 4},
                {4, 3},
                {4, 2},
                {4, 1},
                {3, 1},
                {2, 1},
                {1, 1},
                {0, 2},
        };
        return seedsMap;
    }

    public int[][] SPACESHIPUP() {
        int[][] seedsMap = {
                {0, 0},
                {1, 0},
                {2, 0},
                {0, 1},
                {3, 1},
                {0, 2},
                {0, 3},
                {1, 4},
                {3, 4},
        };
        return seedsMap;
    }

    public int[][] SPACESHIPLEFT() {
        int[][] seedsMap = {
                {1, 0},
                {4, 0},
                {0, 1},
                {0, 2},
                {4, 2},
                {0, 3},
                {1, 3},
                {2, 3},
                {3, 3},
        };
        return seedsMap;
    }

    public int[][] GLIDERGUN() {
        int[][] seedsMap = {
                {1, 5},
                {2, 5},
                {1, 6},
                {2, 6},
                {11, 5},
                {11, 6},
                {11, 7},
                {12, 4},
                {13, 3},
                {14, 3},
                {12, 8},
                {13, 9},
                {14, 9},
                {15, 6},
                {16, 4},
                {17, 5},
                {17, 6},
                {17, 7},
                {18, 6},
                {16, 8},
                {21, 3},
                {21, 4},
                {21, 5},
                {22, 3},
                {22, 4},
                {22, 5},
                {23, 2},
                {23, 6},
                {25, 1},
                {25, 2},
                {25, 6},
                {25, 7},
                {35, 3},
                {35, 4},
                {36, 3},
                {36, 4}
        };
        return seedsMap;
    }
}
