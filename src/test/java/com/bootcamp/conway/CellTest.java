package com.bootcamp.conway;

import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class CellTest {
    @Test
    public void shouldHaveCellWithPosition() {
        Cell cell = new Cell(5, 6);
        int[] expectedPosition = {5, 6};
        assertThat(cell.position(), is(expectedPosition));
    }

    @Test
    public void compareCellWithNullShouldBeFalse() {
        Cell cell = new Cell(1, 1);
        assertThat(cell, is(notNullValue()));
    }

    @Test
    public void compareCellWithOtherThanCellShouldBeFalse() {
        Cell cell = new Cell(1, 1);
        assertThat(cell, is(not("Other")));
    }

    @Test
    public void compareCellWithCellThatHaveDifferentCoordinateShouldBeFalse() {
        Cell cell1 = new Cell(1, 1);
        Cell cell2 = new Cell(0, 0);
        assertThat(cell1, is(not(cell2)));
    }

    @Test
    public void compareCellWithCellThatHaveSameCoordinateShouldBeEqual() {
        Cell cell1 = new Cell(1, 1);
        Cell cell2 = new Cell(1, 1);
        assertThat(cell1, is(cell2));
    }

    @Test
    public void compareCellEqualityWithOtherCellHashCode() {
        Cell cell1 = new Cell(1, 1);
        Cell cell2 = new Cell(1, 1);
        assertThat(cell1.hashCode(), equalTo(cell2.hashCode()));
    }

    @Test
    public void compareCellNotEqualWithOtherCellHashCode() {
        Cell cell1 = new Cell(1, 1);
        Cell cell2 = new Cell(1, 2);
        assertThat(cell1.hashCode(), not(equalTo(cell2.hashCode())));
    }
}