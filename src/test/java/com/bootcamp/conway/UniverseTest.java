package com.bootcamp.conway;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class UniverseTest {
    public Universe universe = new Universe();

    @Test
    public void seedingCellsInUniverseShouldBeValid() {
        int[][] seedsMap = {
                {1, 1}
        };
        ArrayList<Cell> expectedMap = new ArrayList<>();
        expectedMap = universe.seedCells(seedsMap);
        Cell expectedCell = new Cell(1, 1);
        assertThat(expectedMap.contains(expectedCell), is(true));
    }

    @Test
    public void seedingSomeCellsInUniverseShouldBeValid() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2},
                {3, 2},
                {5, 5},
        };
        ArrayList<Cell> expectedMap = new ArrayList<>();
        expectedMap = universe.seedCells(seedsMap);
        Cell expectedCell1 = new Cell(1, 1);
        Cell expectedCell2 = new Cell(2, 2);
        Cell expectedCell3 = new Cell(3, 2);
        Cell expectedCell4 = new Cell(5, 5);
        assertThat(expectedMap.contains(expectedCell1), is(true));
        assertThat(expectedMap.contains(expectedCell2), is(true));
        assertThat(expectedMap.contains(expectedCell3), is(true));
        assertThat(expectedMap.contains(expectedCell4), is(true));
    }

    @Test
    public void shouldBeAblePrintItsCurrentSeeds() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2}
        };
        universe.seedCells(seedsMap);
        assertThat(universe.printUniverse(), is("#-\n-#\n"));
    }

    @Test
    public void shouldBeAbleToPrintNeighbouringData() {
        int[][] seedsMap = {
                {1, 1},
                {2, 2}
        };
        universe.seedCells(seedsMap);
        universe.checkAllNeighbourCount();
        String expectedOutput =
                "0 2 0 = 1\n" +
                "0 1 0 = 1\n" +
                "0 0 0 = 1\n" +
                "3 2 0 = 1\n" +
                "3 1 0 = 1\n" +
                "3 3 0 = 1\n" +
                "1 0 0 = 1\n" +
                "1 1 1 = 1\n" +
                "1 2 0 = 2\n" +
                "1 1 0 = 1\n" +
                "1 3 0 = 1\n" +
                "2 2 1 = 1\n" +
                "2 3 0 = 1\n" +
                "2 2 0 = 1\n" +
                "2 1 0 = 2\n" +
                "2 0 0 = 1\n";
        assertThat(universe.printNeighbouring(), is(expectedOutput));
    }

    @Test
    public void shouldBeAbleTranslateNeighbouringToNewSeed() {
        int[][] seedsMap = {
                {1, 2},
                {2, 2},
                {1, 3},
                {2, 3}
        };
        universe.seedCells(seedsMap);
        int[][] expectedNewSeedMap = {
                {1, 3},
                {1, 2},
                {2, 3},
                {2, 2}
        };
        assertThat(universe.translateNeighbouringToNewSeed(), equalTo(expectedNewSeedMap));
    }
}