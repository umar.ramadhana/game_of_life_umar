#!/usr/bin/env bash

startme() {
    cd /srv/game_of_life 
    ./start.sh input.txt < uin.txt 2> out.error 1> out.result &
    echo $! > run.pid
}

restart_5() {
pid=ps aux | grep "[j]ava -jar build/libs/ConwayGameOfLifeV3.jar input.txt" | cut -d " " -f 4
runtime=
}

stopme() {
  pkill -f "java -jar build/libs/ConwayGameOfLifeV3.jar input.txt"
}

case "$1" in 
    start)   startme ;;
    stop)    stopme ;;
    restart) stopme; startme ;;
    *) echo "usage: $0 start|stop|restart" >&2
       exit 1
       ;;
esac
