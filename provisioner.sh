#!/usr/bin/env bash
sudo yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel wget unzip
sudo mkdir -p /opt/gradle
cd /opt/gradle
wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip
sudo unzip -d /opt/gradle gradle-3.4.1-bin.zip
sudo ln -s /opt/gradle/gradle-3.4.1/bin/gradle /usr/bin/gradle
